
gridtracker (1.24.0512) unstable; urgency=low
  - System: Fixed bug in QSO unique hash algorithm
  -- Tag Loomis <n0ttl@gridtracker.org>  Sun, 12 May 2024 00:00:00 -0000

gridtracker (1.24.0504) unstable; urgency=low
  - System: BIGCTY Update from April 29th (3D2CCC etc)
  - Logging: Threaded log loading and parsing > 500,000 QSOs now supported in real-time
  - Logging: Manual log loads automatically added to startup
  - System: US database refactor
  - Call Roster: Only US states and Canadian provinces recognized
  - Award Tracker: Added RAC: Worked All North America
  - Award Tracker: Added CQ Magazine: WAZ, US-CA, WPX
  - Control Panel: Re-ordered buttons based on online/offline mode
  - Control Panel: Moved 'Award Layer' button to 'Map View Filters > Award' drop-down list
  -- Tag Loomis <n0ttl@gridtracker.org>  Sat, 04 May 2024 00:00:00 -0000

gridtracker (1.24.0104) unstable; urgency=high
  - System - fixed database creation error on new installs
  -- Tag Loomis <n0ttl@gridtracker.org>  Thu, 04 Jan 2024 00:00:00 -0000

gridtracker (1.24.0103) unstable; urgency=low
  - Windows - NSIS installer reverted to 3.06.1-1
  - System - Fixed bug in handling hamqth lookups
  - Stats - DXCCs - Added confirmed band slot counts
  - BIGCTY - Update from Dec 28th 2023
  -- Tag Loomis <n0ttl@gridtracker.org>  Wed, 03 Jan 2024 00:00:00 -0000

gridtracker (1.23.1226) unstable; urgency=low
  - System - Improved Primary Administrative Area (State) detection
  - System - Added Canadian and Australian callsign databases
  - Map - Added - Worked All Canadian Provinces - award layer (Key 9)
  - POTA - Map marker updated when park worked
  - Call Roster - Properly show POTA worked status
  - Logbook - Filter QSO by grid - Working Grid(s)
  -- Tag Loomis <n0ttl@gridtracker.org>  Tue, 26 Dec 2023 00:00:00 -0000

gridtracker (1.23.1217) unstable; urgency=low
  - BIGCTY - Update from December 15th
  - System - Add QSO processing indicator
  - System - QSL location authority selector added in Settings > Logbook
  - Logbook Viewer - QSL sources added
  - Call Roster - Added Grid to Watcher and Ignores
  - Language - Updates to Chinese simplified and traditional translations
  - Logging - DX Keeper grid bug workaround
  -- Tag Loomis <n0ttl@gridtracker.org>  Sun, 17 Dec 2023 00:00:00 -0000

gridtracker (1.23.1207) unstable; urgency=high
 - BIGCTY - Update from December 5th
 - System - LoTW, eQSL and OQRS membership columns icon updated
 - System - Fixed major bug in how confirmation (QSL) records were handled
 - Call Roster - Returned non-slider based exceptions to the exceptions control area
 - Language - Updates to Chinese simplified and traditional translations

 -- Tag Loomis <n0ttl@gridtracker.org>  Thu, 07 Dec 2023 00:00:00 -0000

gridtracker (1.23.1202) unstable; urgency=low
 - BIGCTY - Update from November 29th
 - System - Returned LoTW, eQSL and OQRS membership columns to log entries
 - System - Fixed initial install window sizes
 - Logbook Viewer - Added DXCC reset view button
 - Call Roster
   - New “Move Column Right” added to right-click Column header 
   - Fixed handling of compound callsigns
   - Fixed saving of Ignored CQ zones and ITU zones
   - New Settings Button (Ctrl-S)
   - New Callsign / Message Watcher (Ctrl-W or Ctrl-O)
   - New Exceptions Settings (Ctrl-E)
   - New Ignores Editor / Viewer (Ctrl-I)
   - New Columns Editor (Ctrl-C)

-- Tag Loomis <n0ttl@gridtracker.org>  Sat, 02 Dec 2023 00:00:00 -0000

gridtracker (1.23.1112) unstable; urgency=low
 - Language - Added Français and Italiano (Special thanks to HB9TIH and HB9SNR)
 - Language - Added Español (Special thanks to KI2D and HI8O) 
 - DXCC - Renamed Macedonia to N. Macedonia
 - System - Fixed 1.25m displaying as 125cm in English
 - Award Tracker - Added ARRL VUCC
 - BIGCTY - Update from November 3rd
 - Map - Improved speed of mouse-over rendering and ordering
 - Map - Grid headers show source (Logbook or Live)
 - Call Roster - Fixed issue where Spot wasn't populating correctly
 - Rotor - All locations can now aim on ctrl-left-click when enabled
 - POTA - Left-click a park (tree) on same band/mode will generate messages in WSJT-X
 - Lookup - Added OAMS user to details
 - Logging - Descriptive error if QRZ.com logging failure
 - Logbook Viewer - Regex enabled callsign searching
 - Settings - Logbook - New "Logbook Items Per Page" slider 
 -- Tag Loomis <n0ttl@gridtracker.org>  Sun, 12 Nov 2023 00:00:00 -0000

gridtracker (1.23.1019) unstable; urgency=low
 - HTTP - Removed timeouts on transactions affecting log downloading and uploading
 -- Tag Loomis <n0ttl@gridtracker.org>  Thu, 19 Oct 2023 00:00:00 -0000

gridtracker (1.23.1015) unstable; urgency=low
 - ADIF – Fixed record loading failure for PSK-Reporter if FREQ present
 - WSPR – Fixed decodes not appearing
 -- Tag Loomis <n0ttl@gridtracker.org>  Sun, 15 Oct 2023 00:00:00 -0000

gridtracker (1.23.1014) unstable; urgency=low
 - Call Roster - Fixed "All Traffic/Only Wanted" filtering
 - Call Roster - Fixed POTA wanted same-day issue
 - Call Roster - Added optional "Rig" column
 - System – Support digital mode Q65
 - System – BIGCTY update from October 7th
 - System – Push notification services Simplepush.io and Pushover.net added (OAMS tab)
 - System – Swaziland renamed to Eswatini
 - Logging – HamZone.cn service is now HamCQ.cn
 - Maps – Toner (online), Terrain, Watercolor and Geography Class no longer public access
 -- Tag Loomis <n0ttl@gridtracker.org>  Sat, 14 Oct 2023 00:00:00 -0000

gridtracker (1.23.0402) unstable; urgency=low
 - Roster - fixed Hunting mode not saving
 - Packager - fixed Arm distribution
 -- Tag Loomis <n0ttl@gridtracker.org>  Sun, 02 Apr 2023 00:00:00 -0000

gridtracker (1.23.0328) unstable; urgency=low
 - Language - added German/Deutsch
 - POTA – fixed disappearing parks, removed auto RBNs 
 - Pin mode – added missing band pins
 - Call Roster - added UTC column for easier even/odd spotting
 - Call Roster – fixed “Spotted Me” exception
 - Call Roster – “Uses LoTW” “Uses eQSL” “Uses OQRS” now inclusive
 - Loggers – added HamZone.cn service
 - OAMS – band activity sums neighboring grids correctly
 - Logbook – “Working Date” now accepts time of day
 - Visual Alerts – in own movable window
 - Map – upgraded to OpenLayers 6.15.1, Heatmap now world-wraps
 - Mac – “Full Stop” no longer spoken at startup
 - Satellite – framework started for satellite tracking
 - BIGCTY – update from March 19th
 -- Tag Loomis <n0ttl@gridtracker.org>  Tue, 28 Mar 2023 00:00:00 -0000

gridtracker (1.23.0206) unstable; urgency=low
  - Bugfix for Turkey zone 1 decodes
  - Heatmap part of “Spots” button, Hotkey H removed
  - GT flags button follow “Map View Filters”
  - OAMS chat messages word wrap correctly
  - Mutli-Rig settings moved to Settings → General
  - Call Roster Settings moved to Call Roster, right-click in Roster or press Ctrl-S
  - Call Roster Compact Mode displays Band if multi-band
  - Call Roster Compact Mode entity selection in Call Roster Settings, default “DXCC”
  - Added Call Roster Window Filters in Call Roster Settings, Ctrl-R to reset filters
  - Call Roster POTA column can now sort
  - Added OAMS based band activity
  - Award Tracker CQ Zone and State fix
  - <...> treated as UNKNOWN in Call Roster
  - LoTW download button not responding
 -- Tag Loomis <n0ttl@gridtracker.org>  Mon, 06 Feb 2023 00:00:00 -0000

gridtracker (1.23.0110) unstable; urgency=high
  - Emergency LoTW fix
 -- Tag Loomis <n0ttl@gridtracker.org>  Tue, 10 Jan 2023 00:00:00 -0000

gridtracker (1.22.1226) unstable; urgency=low
  - Fixed lightning strikes not showing on map
  - Fixed distance in status panel
  - Fixed chat window text entry
  - Fixed Logbook of The World loading 
  - No longer clear data on new versions
  - More code clean up
 -- Tag Loomis <n0ttl@gridtracker.org>  Mon, 26 Dec 2022 00:00:00 -0000

gridtracker (1.22.1204) unstable; urgency=low
  - Fixed CPU usage issue with messaging
  - Fixed Logging -> Local File(s) not showing files selected
  - Honor POTA spot expiration
  - New option 'Clear DX Call, DX Grid, Tx1-Tx5 when calling CQ in WSJT-X'
  -- Settings -> Lookups -> Feature Control -> Clear on CQ
 -- Tag Loomis <n0ttl@gridtracker.org>  Sun, 04 Dec 2022 00:00:00 -0000

gridtracker (1.22.1123) unstable; urgency=low
  - LoTW QSL downloading issue fixed
  - CQ Only in Call Roster fixed
  - Failure to start on some installs
 -- Tag Loomis <n0ttl@gridtracker.org>  Wed, 23 Nov 2022 00:00:00 -0000

gridtracker (1.22.1122) unstable; urgency=low
  - WSPR decoding corrupting QSO data fix!
 -- Tag Loomis <n0ttl@gridtracker.org>  Tue, 22 Nov 2022 00:00:00 -0000

gridtracker (1.22.1119) unstable; urgency=low
  - New Internationalization support (Settings → General → Language)
  -- English, 简体中文, 繁體中文 with German and Spanish in the works
  - Removed QRT POTA Spots
  - CatRotator / PstRotator support
  -- Settings → Logging → scroll to bottom to enable
  -- Right-click callsign in Call Roster to Aim
  -- Ctrl-Left-click in Map to Aim
  - Improved callsign validation in Call Roster
  - Grid-overlay (Hotkey B) performance boost
  - BIGCTY update from November 14th
  - Performance improvements
 -- Tag Loomis <n0ttl@gridtracker.org>  Sat, 19 Nov 2022 12:00:00 -0000

gridtracker (1.22.1016) unstable; urgency=low
  - Fix issue with map blanking when upgrading
  - Fix some POTA callers not showing in call roster
  - Fix DXCC none (/MM) not showing in call roster
  - New wanted callsign Regex in call roster
  - New 'No Unknown DXCC' exception in call roster
  - Award tracker now shows wanted options for highlighting
  - Small performance improvements and installation size reduction
 -- Matthew Chambers <nr0q@gridtracker.org>  Sun, 16 Oct 2022 19:55:00 -0000

gridtracker (1.22.1010) unstable; urgency=low
  - Fix issue with map selection not saving/restoring
 -- Tag Loomis <n0ttl@gridtracker.org>  Sun, 09 Oct 2022 12:00:00 -0000

gridtracker (1.22.1009) unstable; urgency=low
  - Fix issue loading logs and generating scores tab
 -- Tag Loomis <n0ttl@gridtracker.org>  Sat, 08 Oct 2022 12:00:00 -0000

gridtracker (1.22.1008) unstable; urgency=low
  - Fix missing callsigns in mh-root when importing BIGCTY

 -- Matthew Chambers <nr0q@gridtracker.org>  Fri, 07 Oct 2022 17:19:00 -0000
 
gridtracker (1.22.1006) unstable; urgency=low
  - Main App:
    - New option in Settings -> Lookups -> Feature Control -> POTA.  When "Enabled", track POTA spots from https://pota.app and auto-report activations. When "Menu Button" checked and menu button active, show active parks following "Map View Filters" rules.
    - OAMS spotting fixed for multiple-instances (Resolves Issue #170)
    - St. Maarten continent changed from SA to NA
    - DXCC none ( ie:  CALL/MM ) no longer counted in Scores or DXCCs stats tabs (Resolves Issue #162)
    - After long window minimize, restore looks correct.
    - Fixed dupe check for last QSO partner (chasing bands, not logging)
    - Prefixes and direct callsign updates from BIGCTY 2022-09-20 (Resolves Issue #144)
    - Fix issue where logged contacts with certain DXCCs on certain bands show as having worked other entities on 630m (Resolves Issue #134)
    - Fix NEXRAD not updating (Resolves Issue #138)
    - Fix buttons not graying when feature toggled off (Resolves Issue #97)
  - Call Roster:
    - Fixed right-click on DXCC not showing DXCC ignore menu (Resolves Issue #153)
    - Fixed "Clear Call Roster On Band Change" bug that caused the roster to blank
    - Fixed Age column not sorting correctly
    - Fixed County cell not doing lookups when allowed
    - Changed County column to show number of potential counties for a callsign
    - Add "RR73 as CQ" to Exceptions
    - Added 4 new "Masters of Radio Communications" awards from QRZ.com to Award Tracker
    - Award Tracker now shows in the Wanted cell the first award that meets criteria
    - Instances now horizontal between controls and roster table (Resolves Issue #166)
    - New option in Settings -> Call Roster -> "Call Roster Delay On Focus".. when enabled will delay redraw of roster after decode round(s) (Resolves Issue #69)

  -- Matthew Chambers <nr0q@gridtracker.org>  Fri, 07 Oct 2022 01:01:00 -0000

gridtracker (1.22.0903) unstable; urgency=low
  - Fixed a bug that displayed 1.25m band QSOs incorrectly.
  - Fixed broken DXCC CQ highlighting and Statistics CQ highlighting.
  - Fixed an exception that occurs when the OAMS server is restarting.
  - Resolved #159 where the Wade Hampton Census area should be the Kusilvak Census Area.
  - Fixed where using the Award Tracker didn't override and hide the Wanted select of the call roster. This was the behavior prior to the call roster refactor.
  - Fix lightening strike display/alerts, this data now flows through OAMS rather then trying to poll direct from Blitzkrieg. 

  -- Matthew Chambers <nr0q@gridtracker.org>  Sat, 03 Sep 2022 15:26:00 -0000

gridtracker (1.22.0725) unstable; urgency=low
  - Resolved #9 Call roster columns order can be changed
  - Resolved $95 Puts calling/called stations at the top of the call roster if sorting by Wanted
  - Resolved #118 Introduce POTA hunting in the call roster
  - Resolved #133 Fixes missing CloudLog Station Profile ID
  - Resolved #150 Highlights RR73/73 the same as a station calling CQ
  - Fixes pattern match for US 1x1 callsigns to match actual FCC rules around them.
  - Add WSJT-X/JTDX active instance name to roster window title when operating with multiple instances.

  -- Matthew Chambers <nr0q@gridtracker.org>  Sun, 24 Jul 2022 19:05:00 -0000

gridtracker (1.22.0503) unstable; urgency=low
  - Increment version for build with correct NWJS version
  
  -- Matthew Chambers <nr0q@gridtracker.org>  Mon, 01 May 2022 16:07:00 -0000
  
gridtracker (1.22.0502) unstable; urgency=low
  [Bug Fixes]
  - Fixed broken Call Roster due to online assets being moved from a web server to Google Storage Bucket.
  - Don't highlight "CQ" rows if filtering by "CQ Only".
  - Resolved #126 Windows Installer script updated to fix issues with install location and missing registry keys
  - Resolved #124 removing IP-Geolocation when no all other means of locating failed, we now tell the user to
    start WSJT-X or enter a location  as Geo-Location services are costly and unreliable
  - Resolved #137 missing libatomic dependency in Linux DEB and RPM spec files
  [Enhancements]
  - Include version number in main window title
  - Call Roster colums refactored and wanted column added
  
 -- Matthew Chambers <nr0q@gridtracker.org>  Mon, 01 May 2022 02:25:40 -0000

gridtracker (1.21.1217) unstable; urgency=low
  - Changed to newer NWJS to fix upstream bug that caused media playback to fail.

 -- Matthew Chambers <nr0q@gridtracker.org>  Fri, 17 Dec 2021 20:08:00 -0000
 
gridtracker (1.21.1212) unstable; urgency=low
  Release build with the call roster refactor code that's been in the works for some time.
  [Bug Fixes]
  - Fix #76, unfinished ignore CQ and ITU zones.
  - Improved handling of stations that are not in a valid DXCC (ie; /MM stations)
  - Improved handling of free text decodes that don't contain valid callsigns (ie "HI BOB" and "MERRY XMAS")
  - Fix how the Call Roster title bar counts are calculated.
  [Enhancements]
  - More clarity when a ULS Zip code falls in more then one county, replacing ~ with ? symbols and
    better tool tip message.
  - Fix #107, where the call roster timeout was longer then a single FT4 cycle.
  - Fix #91, CQ is always highlighted, no matter status of CQ Only.
  - Performance improvement by changing how call roster vars are handled ('let' vs 'var')
  - Build system improved to push to Arch AUR, building of Debian (.deb) packages and triggering
    of COPR RPM builds for Fedora/Cent/RHEL and their cousins.

 -- Matthew Chambers <nr0q@gridtracker.org>  Thu, 12 Dec 2021 15:10:00 -0000

gridtracker (1.21.0928) unstable; urgency=medium
  [Bug Fixes]
  - Treat ADIF record values as byte length vs string length (to better handle UTSF-8 data).
  - Remove looking at fetched records for last date for LoTW fetches, Use only headers (More reliable LoTW fetches).
  [Enhancements]
  - ARM builds now with NWJS 0.54.2 and 64 bit ARM binaries.

 -- Matthew Chambers <nr0q@gridtracker.org>  Sun, 28 Sep 2021 00:00:00 -0000

gridtracker (1.21.0620) unstable; urgency=medium
  [Bug Fixes]
  - Fix pulling down of LoTW logs at start-up with a differential log syncing mechanism that only get's changes since last sync, also cool down timer to prevent rapid reloading of LoTW log. 
  [Enhancements]
  - Automatic pulling down of acknowledgements.json file daily when doing version check (if enabled). 

 -- Matthew Chambers <nr0q@gridtracker.org>  Sat, 19 Jun 2021 16:49:00 -0000
 
gridtracker (1.21.0613) unstable; urgency=medium
  [Bug Fixes]
  - Fix pulling down LoTW log at start-up causing issue for other programs that are trying to sync LoTW logs.
  [Enhancements]
  - Updated list of contributors

 -- Matthew Chambers <nr0q@gridtracker.org>  Sun, 13 Jun 2021 03:04:00 -0000

gridtracker (1.21.0530) unstable; urgency=medium
  [Bug Fixes]
  - Fix spots layer not refreshing/clearing when TX idle
  - Mark /MM as not DXCC per ARRL DXCC rules
  - Fix bug that causes call roster to stick
  - Fix typo in Call Roster "OAMS" heading
  - Fix DXCC GeoJSON centers that broke DXCC based map pathes
  - Fix mislabel of Eswatini
  [Enhancements]
  - Add statistical information to call roster title bar

 -- Matthew Chambers <nr0q@gridtracker.org>  Sat, 30 May 2021 00:10:00 -0000

gridtracker (1.21.0520) unstable; urgency=medium
  [Bug Fixes]
  - Fix PSK Reporter poll time to 5min , add TX idle timeout and time skew to reduce the load GridTracker makes on the PSK Reporter server
  - Fix SVG icons not rendering on certain Windows 10 installs
  - Fix eQSL ADIF support
  - Fix clearing of logs after installing new version of GridTracker
  - Fix hightlighting whole country of Somalia on map
  [Enhancements]
  - Update Award Tracker with new FT8DMC and European ROS Club awards
  - Improvements to the callroster
  - Add recognition of contributors to GridTracker within the Call Roster and lookup window
  - Make settings icon a toggle that both opens and closes the settings pane
  - Grid and IP Address Fields are slightly wider

 -- Matthew Chambers <nr0q@gridtracker.org>  Mon, 17 May 2021 02:30:00 -0000

gridtracker (1.21.0407) unstable; urgency=medium
  [Bug Fixes]
  - mp3 alerts from previous versions now work correctly
  - callook lookup preference now stored
  - call roster WSJT-X/JTDX instance label/checkbox overlap fixed
  [Enhancements]
  - new icon to request ClubLog OQRS QSL
  - add eQSL check in log file processing

 -- Matthew Chambers <nr0q@gridtracker.org>  Wed, 07 Apr 2021 00:00:00 -0000

gridtracker (1.21.0327) unstable; urgency=medium
  This is the public release of the 1.21.0324 hotfix release candidates

 -- Matthew Chambers <nr0q@gridtracker.org>  Fri, 27 Mar 2021 00:38:00 -0000

gridtracker (1.21.0324) unstable; urgency=medium
  [Christian Bayer]
  * Fixed #72 CR not filtering correctly
  * Fixed #63 blurry Windows icon
  [Matthew Chambers]
  * Fixed #71 bug with loading adif files
  This is the hotfix release of the first public release of 1.21.0322

 -- Christian Bayer <chrbayer84@googlemail.com>  Wed, 22 Mar 2021 22:00:00 -0500

gridtracker (1.21.0322) unstable; urgency=medium
  [Christian Bayer]
  * Fixed windows packaging 
  [Matthew Chambers]
  * Fixed linux and arm packaging
  This is the public release of 1.21.0307 release candidates

 -- Matthew Chambers <nr0q@gridtracker.org>  Mon, 22 Mar 2021 20:30:00 -0500

gridtracker (1.21.0307) unstable; urgency=medium

  [ Paul Traina ]
  * Cleaned up the title bar to show context information
  * GT now recognizes JTDX on Mac
  * PSK-Spot layer merged with Live/Logbook Grids by default
  * Inputs in settings sanitized
  * Fix spots when in realtime mode
  * Added CQ zone name in reports
  [ Tag Loomis ]
  * Roster context menu active on entire window
  * remote station distance and azimuth in lookup if available
  [ Sebastian Delmont ]
  * Changed layout of roster controls and made it simpler to hide and show them
  * Allow roster window to be as narrow as WSJT-X narrowest window size
  [ Mattew Chambers ]
  * Call Roster is now labeled "GridTracker" and contains partial layer
    information
  [ Christian Bayer ]
  * Fixed ordering of centimeter bands in stats window
  * Clicking an unconfirmed DXCC in stats window now shows relevant log entries
    in pop up window
  * New Callook preference setting. if checked, US callsigns will always be
    queried from Callook since it usually has more data than free QRZ lookup
  * fixed CR alert script not being triggered for Awared Tracker hits

 -- Matthew Chambers <nr0q@gridtracker.org>  Sun, 7 Mar 2021 12:00:00 -0000

gridtracker (1.20.1118) unstable; urgency=low

  * GridTracker is now Open Source! Copyright assigned to GridTracker.org and
    is BSD 3-clause.
  [ Paul Traina ]
  * Disconnect from tagloomis.com infrastructure -> gridtracker.org
  * Disable fit to QTH when in PSK mode.
  * Highlight confirmed DXCC countries in DXCC report.
  * Unify worked/confirmed/unworked in WAC/WAS/CQ Zones/ITU Zones reports.
  * In call roster, when requiring LoTW, don't show stations that don't meet
    time limits.
  * Improve call roster title bar.
  * Fix duplicate-first-name in lookup window.
  * Change ADIF COUNTRY field output to comply with ADIF 3.1.1 specification.
  * Clean up media handling.
    Don't create duplicates of GT's media files in Documents/GridTracker/media.
    Remove any duplicates that are already there.
    User directory is still respected if you want to add your own files there.
  * Add a man page for GridTracker for Linux packages.
  * Add RPM build support, based upon NR0Q's work.
  * Support auto-building CI/CD for GitLab, Debian, and RPM packaging
  [ Sebastian Delmont ]
  * Improve roster controls
  * Clean up and pretify HTML and JSON code, reformat code base.
  * Make UDP port vaildation behavior clearer, allow receive on 2238 if
    forwarding enabled.
  * Improve debugging/developer experience by enabling context menus when
    using nwjs's SDK.

 -- Paul Traina <216482-pleasantone@users.noreply.gitlab.com>  Wed, 04 Nov 2020 15:58:29 -0800

gridtracker (1.20.0927+repack1) unstable; urgency=high

  * Clean up nw execution in .desktop and .sh file.

 -- Paul Traina <216482-pleasantone@users.noreply.gitlab.com>  Wed, 07 Oct 2020 09:16:02 -0700

gridtracker (1.20.0927) unstable; urgency=medium

  [ Tag Loomis ]
  * Author release 1.20.0927

  [ Paul Traina ]
  * Debian package building support

 -- Paul Traina <216482-pleasantone@users.noreply.gitlab.com>  Mon, 28 Sep 2020 14:06:49 -0700
