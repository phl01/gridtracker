let languages = {
  en: "i18n/en.json",
  cn: "i18n/cn.json",
  cnt: "i18n/cn-t.json",
  de: "i18n/de.json",
  fr: "i18n/fr.json",
  it: "i18n/it.json",
  es: "i18n/es.json"
};

function loadI18n()
{
  $.i18n().load(languages).done(function ()
  {
    $.i18n().locale = GT.appSettings.locale;
    refreshI18NStrings();
  });
}

function renderI18n(locale)
{
  $.i18n().locale = locale;
  $("body").i18n();
}

function changeLocale()
{
  GT.appSettings.locale = languageLocale.value;
  renderI18n(GT.appSettings.locale);
  saveAndCloseApp(true);
}

function loadChildWindowI18n()
{
  $.i18n().load(languages).done(function ()
  {
    renderI18n(window.opener.GT.appSettings.locale);
  });
}

function loadRosterI18n()
{
  $.i18n().load(languages).done(function ()
  {
    renderI18n(window.opener.GT.appSettings.locale);
    addControls();
  });
}

function renderLocale()
{
  renderI18n(GT.appSettings.locale);
}
